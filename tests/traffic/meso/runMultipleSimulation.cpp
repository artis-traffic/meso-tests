/*
 * @file tests/meso/runMultipleSimulation.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-star/common/RootCoordinator.hpp>
#include <artis-star/common/observer/Output.hpp>
#include <artis-star/common/observer/TimedIterator.hpp>

#include "links_def.hpp"

SimpleNetworkParameters getNewParameters(double i) {
  SimpleNetworkParameters params = {{0,                  i * 0.95, i,                  i * 1.1,    0.01,               432451,             {{0, 0, 0}, {0, 1, 0}}},
                                    {10000,              i * 0.95, i,                  i * 1.1,    0.01,               432452,             {{0, 0, 0}, {0, 1, 0}}},
                                    {20000,              i * 0.95, i,                  i * 1.1,    0.01,               432453,             {{0, 0, 0}, {0, 1, 0}}},
                                    {838.9045422558034,  2, 25,                 0.12, 6.03448275862069,
                                                                                                          1.1666666666666667, 2, 1},
                                    {325.76532757685436, 1, 13.88888888888889,  0.14,
                                                                                      3.7878787878787876, 1.1666666666666667, 1, 2},
                                    {649.5288300368213,  1, 13.88888888888889,  0.14,
                                                                                      3.7878787878787876, 1.1666666666666667, 0, 3},
                                    {296.1776578416324,  2, 30.555555555555554, 0.12,
                                                                                      5.780780780780781,  4,                2, 4},
                                    {385.0239747913269,  1, 13.88888888888889,  0.14,
                                                                                      3.7878787878787876, 0.1,                1, 5},
                                    {470.47634439116223, 2, 30.555555555555554, 0.14,
                                                                                      5.780780780780781,  0.1,                1, 6},
                                    {521.1159478447717,  2, 30.555555555555554, 0.12,
                                                                                      5.780780780780781,  1.1666666666666667, 0, 7},
                                    {308.4873243331674,  2, 30.555555555555554, 0.12,
                                                                                      5.780780780780781,  1.1666666666666667, 2, 8},
                                    {788.416249620489,   1, 13.88888888888889,  0.14,
                                                                                      3.7878787878787876, 0.4166666666666667, 1, 9},
                                    {742.0905061522807,  2, 25,                 0.12, 6.03448275862069,
                                                                                                          1.1666666666666667, 0, 10},
                                    {269.5449579184339,  2, 30.555555555555554, 0.12,
                                                                                      5.780780780780781,  1.1666666666666667, 1, 11},
                                    {701.7882356491866,  2, 30.555555555555554, 0.12,
                                                                                      5.780780780780781,  1.1666666666666667, 0, 12},
                                    {0.,                 1, 2,                  {1}},
                                    {0.,                 1, 1,                  {1}},
                                    {0.,                 2, 1,                  {0.75, 0.25}},
                                    {0.,                 1, 2,                  {1}},
                                    {0.,                 1, 2,                  {1}},
                                    {0.,                 2, 1,                  {0.5,  0.5}}};
  return params;
}

void runSimulation(SimpleNetworkParameters &parameters, unsigned int iteration) {
  artis::common::context::Context<artis::common::DoubleTime> context(25200, 28800);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime,
      SimpleNetworkGraphManager,
      SimpleNetworkParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

  rc.attachView("System" + std::to_string(iteration), new TwoLinkWithNodeView);

  rc.switch_to_timed_observer(0.1);

  rc.run(context);

  artis::common::observer::Output<artis::common::DoubleTime,
    artis::common::observer::TimedIterator<artis::common::DoubleTime>>
    output(rc.observer());

  output(context.begin(), context.end(), {context.begin(), 0.1});
}

int main() {
  unsigned int index = 1;
  for (double i = 0.1; i < 2; i += 0.02) {
    SimpleNetworkParameters parameters = getNewParameters(i);

    std::cout << "Simulation " << index << std::endl;

    runSimulation(parameters, index++);
  }
  return 0;
}