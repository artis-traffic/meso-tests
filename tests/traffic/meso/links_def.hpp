/**
 * @file tests/meso/links_def.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_TESTS_MESO_LINKS_DEF_HPP
#define ARTIS_TRAFFIC_TESTS_MESO_LINKS_DEF_HPP

#include <artis-star/kernel/pdevs/Coordinator.hpp>
#include <artis-star/kernel/pdevs/GraphManager.hpp>
#include <artis-star/kernel/pdevs/Simulator.hpp>

#include <artis-traffic/meso/core/Link.hpp>
#include <artis-traffic/meso/core/LinkQueue.hpp>
#include <artis-traffic/meso/core/LinkTravel.hpp>
#include <artis-traffic/meso/core/Node.hpp>
#include <artis-traffic/meso/utils/Counter.hpp>
#include <artis-traffic/meso/utils/Generator.hpp>

struct SimpleNetworkParameters {
  artis::traffic::meso::utils::GeneratorParameters generator_1_parameters;
  artis::traffic::meso::utils::GeneratorParameters generator_8_parameters;
  artis::traffic::meso::utils::GeneratorParameters generator_11_parameters;
  artis::traffic::meso::core::LinkParameters link_1_parameters;
  artis::traffic::meso::core::LinkParameters link_2_parameters;
  artis::traffic::meso::core::LinkParameters link_3_parameters;
  artis::traffic::meso::core::LinkParameters link_4_parameters;
  artis::traffic::meso::core::LinkParameters link_5_parameters;
  artis::traffic::meso::core::LinkParameters link_6_parameters;
  artis::traffic::meso::core::LinkParameters link_7_parameters;
  artis::traffic::meso::core::LinkParameters link_8_parameters;
  artis::traffic::meso::core::LinkParameters link_9_parameters;
  artis::traffic::meso::core::LinkParameters link_10_parameters;
  artis::traffic::meso::core::LinkParameters link_11_parameters;
  artis::traffic::meso::core::LinkParameters link_12_parameters;
  artis::traffic::meso::core::NodeParameters node_3_parameters;
  artis::traffic::meso::core::NodeParameters node_4_parameters;
  artis::traffic::meso::core::NodeParameters node_5_parameters;
  artis::traffic::meso::core::NodeParameters node_6_parameters;
  artis::traffic::meso::core::NodeParameters node_9_parameters;
  artis::traffic::meso::core::NodeParameters node_10_parameters;
};

class SimpleNetworkGraphManager :
  public artis::pdevs::GraphManager<artis::common::DoubleTime, SimpleNetworkParameters> {
public:
  enum sub_models {
    GENERATOR_1,
    GENERATOR_8,
    GENERATOR_11,
    LINK_1,
    LINK_2,
    LINK_3,
    LINK_4,
    LINK_5,
    LINK_6,
    LINK_7,
    LINK_8,
    LINK_9,
    LINK_10,
    LINK_11,
    LINK_12,
    NODE_3,
    NODE_4,
    NODE_5,
    NODE_6,
    NODE_9,
    NODE_10,
    COUNTER_2,
    COUNTER_4,
    COUNTER_7,
    COUNTER_12
  };

  SimpleNetworkGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                            const SimpleNetworkParameters &parameters,
                            const artis::common::NoParameters &graph_parameters)
    :
    artis::pdevs::GraphManager<artis::common::DoubleTime, SimpleNetworkParameters>(
      coordinator, parameters, graph_parameters),
    _generator_1("generator_1", parameters.generator_1_parameters),
    _generator_8("generator_8", parameters.generator_8_parameters),
    _generator_11("generator_11", parameters.generator_11_parameters),
    _link_1("L1", parameters.link_1_parameters, graph_parameters),
    _link_2("L2", parameters.link_2_parameters, graph_parameters),
    _link_3("L3", parameters.link_3_parameters, graph_parameters),
    _link_4("L4", parameters.link_4_parameters, graph_parameters),
    _link_5("L5", parameters.link_5_parameters, graph_parameters),
    _link_6("L6", parameters.link_6_parameters, graph_parameters),
    _link_7("L7", parameters.link_7_parameters, graph_parameters),
    _link_8("L8", parameters.link_8_parameters, graph_parameters),
    _link_9("L9", parameters.link_9_parameters, graph_parameters),
    _link_10("L10", parameters.link_10_parameters, graph_parameters),
    _link_11("L11", parameters.link_11_parameters, graph_parameters),
    _link_12("L12", parameters.link_12_parameters, graph_parameters),
    _node_3("node_3", parameters.node_3_parameters),
    _node_4("node_4", parameters.node_4_parameters),
    _node_5("node_5", parameters.node_5_parameters),
    _node_6("node_6", parameters.node_6_parameters),
    _node_9("node_9", parameters.node_9_parameters),
    _node_10("node_10", parameters.node_10_parameters),
    _counter_2("counter_2", artis::common::NoParameters()),
    _counter_4("counter_4", artis::common::NoParameters()),
    _counter_7("counter_7", artis::common::NoParameters()),
    _counter_12("counter_12", artis::common::NoParameters()) {
    add_child(GENERATOR_1, &_generator_1);
    add_child(GENERATOR_8, &_generator_8);
    add_child(GENERATOR_11, &_generator_11);
    add_child(LINK_1, &_link_1);
    add_child(LINK_2, &_link_2);
    add_child(LINK_3, &_link_3);
    add_child(LINK_4, &_link_4);
    add_child(LINK_5, &_link_5);
    add_child(LINK_6, &_link_6);
    add_child(LINK_7, &_link_7);
    add_child(LINK_8, &_link_8);
    add_child(LINK_9, &_link_9);
    add_child(LINK_10, &_link_10);
    add_child(LINK_11, &_link_11);
    add_child(LINK_12, &_link_12);
    add_child(NODE_3, &_node_3);
    add_child(NODE_4, &_node_4);
    add_child(NODE_5, &_node_5);
    add_child(NODE_6, &_node_6);
    add_child(NODE_9, &_node_9);
    add_child(NODE_10, &_node_10);
    add_child(COUNTER_2, &_counter_2);
    add_child(COUNTER_4, &_counter_4);
    add_child(COUNTER_7, &_counter_7);
    add_child(COUNTER_12, &_counter_12);

    out({&_generator_11, artis::traffic::meso::utils::Generator::outputs::OUT})
      >> in({&_link_1, artis::traffic::meso::core::LinkGraphManager::inputs::IN});
    out({&_link_1, artis::traffic::meso::core::LinkGraphManager::outputs::OUT_OPEN})
      >> in({&_generator_11, artis::traffic::meso::utils::Generator::inputs::IN_OPEN});
    out({&_link_1, artis::traffic::meso::core::LinkGraphManager::outputs::OUT_CLOSE})
      >> in({&_generator_11, artis::traffic::meso::utils::Generator::inputs::IN_CLOSE});
    out({&_link_1, artis::traffic::meso::core::LinkGraphManager::outputs::OUT})
      >> in({&_node_9, artis::traffic::meso::core::Node::inputs::IN});
    out({&_node_9, artis::traffic::meso::core::Node::outputs::OUT_CLOSE})
      >> in({&_link_1, artis::traffic::meso::core::LinkGraphManager::inputs::IN_CLOSE});
    out({&_node_9, artis::traffic::meso::core::Node::outputs::OUT_OPEN})
      >> in({&_link_1, artis::traffic::meso::core::LinkGraphManager::inputs::IN_OPEN});
    out({&_node_9, artis::traffic::meso::core::Node::outputs::OUT_CONFIRM})
      >> in({&_link_1, artis::traffic::meso::core::LinkGraphManager::inputs::IN_CONFIRM_OUT});
    out({&_node_9, artis::traffic::meso::core::Node::outputs::OUT})
      >> in({&_link_2, artis::traffic::meso::core::LinkGraphManager::inputs::IN});
    out({&_link_2, artis::traffic::meso::core::LinkGraphManager::outputs::OUT_OPEN})
      >> in({&_node_9, artis::traffic::meso::core::Node::inputs::IN_OPEN});
    out({&_link_2, artis::traffic::meso::core::LinkGraphManager::outputs::OUT_CLOSE})
      >> in({&_node_9, artis::traffic::meso::core::Node::inputs::IN_CLOSE});
    out({&_link_2, artis::traffic::meso::core::LinkGraphManager::outputs::OUT})
      >> in({&_node_5, artis::traffic::meso::core::Node::inputs::IN});
    out({&_node_5, artis::traffic::meso::core::Node::outputs::OUT_CLOSE})
      >> in({&_link_2, artis::traffic::meso::core::LinkGraphManager::inputs::IN_CLOSE});
    out({&_node_5, artis::traffic::meso::core::Node::outputs::BACK})
      >> in({&_link_2, artis::traffic::meso::core::LinkGraphManager::inputs::BACK});
    out({&_node_5, artis::traffic::meso::core::Node::outputs::OUT_OPEN})
      >> in({&_link_2, artis::traffic::meso::core::LinkGraphManager::inputs::IN_OPEN});
    out({&_node_5, artis::traffic::meso::core::Node::outputs::OUT_CONFIRM})
      >> in({&_link_2, artis::traffic::meso::core::LinkGraphManager::inputs::IN_CONFIRM_OUT});
    out({&_node_5, artis::traffic::meso::core::Node::outputs::OUT})
      >> in({&_link_7, artis::traffic::meso::core::LinkGraphManager::inputs::IN});
    out({&_link_7, artis::traffic::meso::core::LinkGraphManager::outputs::OUT_OPEN})
      >> in({&_node_5, artis::traffic::meso::core::Node::inputs::IN_OPEN});
    out({&_link_7, artis::traffic::meso::core::LinkGraphManager::outputs::OUT_CLOSE})
      >> in({&_node_5, artis::traffic::meso::core::Node::inputs::IN_CLOSE});
    out({&_link_7, artis::traffic::meso::core::LinkGraphManager::outputs::OUT})
      >> in({&_counter_2, artis::traffic::meso::core::Node::inputs::IN});
    out({&_counter_2, artis::traffic::meso::utils::Counter::outputs::OUT_CONFIRM})
      >> in({&_link_7, artis::traffic::meso::core::LinkGraphManager::inputs::IN_CONFIRM_OUT});
    out({&_node_9, artis::traffic::meso::core::Node::outputs::OUT + 1})
      >> in({&_link_3, artis::traffic::meso::core::LinkGraphManager::inputs::IN});
    out({&_link_3, artis::traffic::meso::core::LinkGraphManager::outputs::OUT_OPEN})
      >> in({&_node_9, artis::traffic::meso::core::Node::inputs::IN_OPEN + 1});
    out({&_link_3, artis::traffic::meso::core::LinkGraphManager::outputs::OUT_CLOSE})
      >> in({&_node_9, artis::traffic::meso::core::Node::inputs::IN_CLOSE + 1});
    out({&_link_3, artis::traffic::meso::core::LinkGraphManager::outputs::OUT})
      >> in({&_counter_4, artis::traffic::meso::utils::Counter::inputs::IN});
    out({&_counter_4, artis::traffic::meso::utils::Counter::outputs::OUT_CONFIRM})
      >> in({&_link_3, artis::traffic::meso::core::LinkGraphManager::inputs::IN_CONFIRM_OUT});

    out({&_generator_8, artis::traffic::meso::utils::Generator::outputs::OUT})
      >> in({&_link_4, artis::traffic::meso::core::LinkGraphManager::inputs::IN});
    out({&_link_4, artis::traffic::meso::core::LinkGraphManager::outputs::OUT_OPEN})
      >> in({&_generator_8, artis::traffic::meso::utils::Generator::inputs::IN_OPEN});
    out({&_link_4, artis::traffic::meso::core::LinkGraphManager::outputs::OUT_CLOSE})
      >> in({&_generator_8, artis::traffic::meso::utils::Generator::inputs::IN_CLOSE});
    out({&_link_4, artis::traffic::meso::core::LinkGraphManager::outputs::OUT})
      >> in({&_node_6, artis::traffic::meso::core::Node::inputs::IN});
    out({&_node_6, artis::traffic::meso::core::Node::outputs::OUT_CLOSE})
      >> in({&_link_4, artis::traffic::meso::core::LinkGraphManager::inputs::IN_CLOSE});
    out({&_node_6, artis::traffic::meso::core::Node::outputs::OUT_OPEN})
      >> in({&_link_4, artis::traffic::meso::core::LinkGraphManager::inputs::IN_OPEN});
    out({&_node_6, artis::traffic::meso::core::Node::outputs::OUT_CONFIRM})
      >> in({&_link_4, artis::traffic::meso::core::LinkGraphManager::inputs::IN_CONFIRM_OUT});
    out({&_node_6, artis::traffic::meso::core::Node::outputs::OUT})
      >> in({&_link_6, artis::traffic::meso::core::LinkGraphManager::inputs::IN});
    out({&_link_6, artis::traffic::meso::core::LinkGraphManager::outputs::OUT_OPEN})
      >> in({&_node_6, artis::traffic::meso::core::Node::inputs::IN_OPEN});
    out({&_link_6, artis::traffic::meso::core::LinkGraphManager::outputs::OUT_CLOSE})
      >> in({&_node_6, artis::traffic::meso::core::Node::inputs::IN_CLOSE});
    out({&_link_6, artis::traffic::meso::core::LinkGraphManager::outputs::OUT})
      >> in({&_node_5, artis::traffic::meso::core::Node::inputs::IN + 1});
    out({&_link_5, artis::traffic::meso::core::LinkGraphManager::outputs::OUT_OPEN})
      >> in({&_node_6, artis::traffic::meso::core::Node::inputs::IN_OPEN + 1});
    out({&_node_5, artis::traffic::meso::core::Node::outputs::OUT_CLOSE})
      >> in({&_link_6, artis::traffic::meso::core::LinkGraphManager::inputs::IN_CLOSE});
    out({&_node_5, artis::traffic::meso::core::Node::outputs::BACK + 1})
      >> in({&_link_6, artis::traffic::meso::core::LinkGraphManager::inputs::BACK});
    out({&_node_5, artis::traffic::meso::core::Node::outputs::OUT_OPEN})
      >> in({&_link_6, artis::traffic::meso::core::LinkGraphManager::inputs::IN_OPEN});
    out({&_node_5, artis::traffic::meso::core::Node::outputs::OUT_CONFIRM + 1})
      >> in({&_link_6, artis::traffic::meso::core::LinkGraphManager::inputs::IN_CONFIRM_OUT});
    out({&_link_5, artis::traffic::meso::core::LinkGraphManager::outputs::OUT_CLOSE})
      >> in({&_node_6, artis::traffic::meso::core::Node::inputs::IN_CLOSE + 1});
    out({&_node_6, artis::traffic::meso::core::Node::outputs::OUT + 1})
      >> in({&_link_5, artis::traffic::meso::core::LinkGraphManager::inputs::IN});
    out({&_link_5, artis::traffic::meso::core::LinkGraphManager::outputs::OUT})
      >> in({&_node_10, artis::traffic::meso::core::Node::inputs::IN});
    out({&_node_10, artis::traffic::meso::core::Node::outputs::OUT_CLOSE})
      >> in({&_link_5, artis::traffic::meso::core::LinkGraphManager::inputs::IN_CLOSE});
    out({&_node_10, artis::traffic::meso::core::Node::outputs::OUT_OPEN})
      >> in({&_link_5, artis::traffic::meso::core::LinkGraphManager::inputs::IN_OPEN});
    out({&_node_10, artis::traffic::meso::core::Node::outputs::OUT_CONFIRM})
      >> in({&_link_5, artis::traffic::meso::core::LinkGraphManager::inputs::IN_CONFIRM_OUT});
    out({&_node_10, artis::traffic::meso::core::Node::outputs::BACK})
      >> in({&_link_5, artis::traffic::meso::core::LinkGraphManager::inputs::BACK});
    out({&_node_10, artis::traffic::meso::core::Node::outputs::OUT})
      >> in({&_link_10, artis::traffic::meso::core::LinkGraphManager::inputs::IN});
    out({&_link_10, artis::traffic::meso::core::LinkGraphManager::outputs::OUT_OPEN})
      >> in({&_node_10, artis::traffic::meso::core::Node::inputs::IN_OPEN});
    out({&_link_10, artis::traffic::meso::core::LinkGraphManager::outputs::OUT_CLOSE})
      >> in({&_node_10, artis::traffic::meso::core::Node::inputs::IN_CLOSE});
    out({&_link_10, artis::traffic::meso::core::LinkGraphManager::outputs::OUT})
      >> in({&_counter_12, artis::traffic::meso::utils::Counter::inputs::IN});
    out({&_counter_12, artis::traffic::meso::utils::Counter::outputs::OUT_CONFIRM})
      >> in({&_link_10, artis::traffic::meso::core::LinkGraphManager::inputs::IN_CONFIRM_OUT});

    out({&_generator_1, artis::traffic::meso::utils::Generator::outputs::OUT})
      >> in({&_link_8, artis::traffic::meso::core::LinkGraphManager::inputs::IN});
    out({&_link_8, artis::traffic::meso::core::LinkGraphManager::outputs::OUT_OPEN})
      >> in({&_generator_1, artis::traffic::meso::utils::Generator::inputs::IN_OPEN});
    out({&_link_8, artis::traffic::meso::core::LinkGraphManager::outputs::OUT_CLOSE})
      >> in({&_generator_1, artis::traffic::meso::utils::Generator::inputs::IN_CLOSE});
    out({&_link_8, artis::traffic::meso::core::LinkGraphManager::outputs::OUT})
      >> in({&_node_3, artis::traffic::meso::core::Node::inputs::IN});
    out({&_node_3, artis::traffic::meso::core::Node::outputs::OUT_CLOSE})
      >> in({&_link_8, artis::traffic::meso::core::LinkGraphManager::inputs::IN_CLOSE});
    out({&_node_3, artis::traffic::meso::core::Node::outputs::OUT_OPEN})
      >> in({&_link_8, artis::traffic::meso::core::LinkGraphManager::inputs::IN_OPEN});
    out({&_node_3, artis::traffic::meso::core::Node::outputs::OUT_CONFIRM})
      >> in({&_link_8, artis::traffic::meso::core::LinkGraphManager::inputs::IN_CONFIRM_OUT});
    out({&_node_3, artis::traffic::meso::core::Node::outputs::OUT})
      >> in({&_link_9, artis::traffic::meso::core::LinkGraphManager::inputs::IN});
    out({&_link_9, artis::traffic::meso::core::LinkGraphManager::outputs::OUT_OPEN})
      >> in({&_node_3, artis::traffic::meso::core::Node::inputs::IN_OPEN});
    out({&_link_9, artis::traffic::meso::core::LinkGraphManager::outputs::OUT_CLOSE})
      >> in({&_node_3, artis::traffic::meso::core::Node::inputs::IN_CLOSE});
    out({&_link_9, artis::traffic::meso::core::LinkGraphManager::outputs::OUT})
      >> in({&_node_10, artis::traffic::meso::core::Node::inputs::IN + 1});
    out({&_node_10, artis::traffic::meso::core::Node::outputs::OUT_CLOSE})
      >> in({&_link_9, artis::traffic::meso::core::LinkGraphManager::inputs::IN_CLOSE});
    out({&_node_10, artis::traffic::meso::core::Node::outputs::OUT_OPEN})
      >> in({&_link_9, artis::traffic::meso::core::LinkGraphManager::inputs::IN_OPEN});
    out({&_node_10, artis::traffic::meso::core::Node::outputs::OUT_CONFIRM + 1})
      >> in({&_link_9, artis::traffic::meso::core::LinkGraphManager::inputs::IN_CONFIRM_OUT});
    out({&_node_10, artis::traffic::meso::core::Node::outputs::BACK + 1})
      >> in({&_link_9, artis::traffic::meso::core::LinkGraphManager::inputs::BACK});
    out({&_node_3, artis::traffic::meso::core::Node::outputs::OUT + 1})
      >> in({&_link_11, artis::traffic::meso::core::LinkGraphManager::inputs::IN});
    out({&_link_11, artis::traffic::meso::core::LinkGraphManager::outputs::OUT_OPEN})
      >> in({&_node_3, artis::traffic::meso::core::Node::inputs::IN_OPEN + 1});
    out({&_link_11, artis::traffic::meso::core::LinkGraphManager::outputs::OUT_CLOSE})
      >> in({&_node_3, artis::traffic::meso::core::Node::inputs::IN_CLOSE + 1});
    out({&_link_11, artis::traffic::meso::core::LinkGraphManager::outputs::OUT})
      >> in({&_node_4, artis::traffic::meso::core::Node::inputs::IN});
    out({&_node_4, artis::traffic::meso::core::Node::outputs::OUT_CLOSE})
      >> in({&_link_11, artis::traffic::meso::core::LinkGraphManager::inputs::IN_CLOSE});
    out({&_node_4, artis::traffic::meso::core::Node::outputs::OUT_OPEN})
      >> in({&_link_11, artis::traffic::meso::core::LinkGraphManager::inputs::IN_OPEN});
    out({&_node_4, artis::traffic::meso::core::Node::outputs::OUT_CONFIRM})
      >> in({&_link_11, artis::traffic::meso::core::LinkGraphManager::inputs::IN_CONFIRM_OUT});
    out({&_node_4, artis::traffic::meso::core::Node::outputs::OUT})
      >> in({&_link_12, artis::traffic::meso::core::LinkGraphManager::inputs::IN});
    out({&_link_12, artis::traffic::meso::core::LinkGraphManager::outputs::OUT_OPEN})
      >> in({&_node_4, artis::traffic::meso::core::Node::inputs::IN_OPEN});
    out({&_link_12, artis::traffic::meso::core::LinkGraphManager::outputs::OUT_CLOSE})
      >> in({&_node_4, artis::traffic::meso::core::Node::inputs::IN_CLOSE});
    out({&_link_12, artis::traffic::meso::core::LinkGraphManager::outputs::OUT})
      >> in({&_counter_7, artis::traffic::meso::utils::Counter::inputs::IN});
    out({&_counter_7, artis::traffic::meso::utils::Counter::outputs::OUT_CONFIRM})
      >> in({&_link_12, artis::traffic::meso::core::LinkGraphManager::inputs::IN_CONFIRM_OUT});
  }

  ~SimpleNetworkGraphManager() override = default;

private:
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::meso::utils::Generator,
    artis::traffic::meso::utils::GeneratorParameters> _generator_1;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::meso::utils::Generator,
    artis::traffic::meso::utils::GeneratorParameters> _generator_8;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::meso::utils::Generator,
    artis::traffic::meso::utils::GeneratorParameters> _generator_11;
  artis::pdevs::Coordinator<artis::common::DoubleTime,
    artis::traffic::meso::core::LinkGraphManager,
    artis::traffic::meso::core::LinkParameters> _link_1;
  artis::pdevs::Coordinator<artis::common::DoubleTime,
    artis::traffic::meso::core::LinkGraphManager,
    artis::traffic::meso::core::LinkParameters> _link_2;
  artis::pdevs::Coordinator<artis::common::DoubleTime,
    artis::traffic::meso::core::LinkGraphManager,
    artis::traffic::meso::core::LinkParameters> _link_3;
  artis::pdevs::Coordinator<artis::common::DoubleTime,
    artis::traffic::meso::core::LinkGraphManager,
    artis::traffic::meso::core::LinkParameters> _link_4;
  artis::pdevs::Coordinator<artis::common::DoubleTime,
    artis::traffic::meso::core::LinkGraphManager,
    artis::traffic::meso::core::LinkParameters> _link_5;
  artis::pdevs::Coordinator<artis::common::DoubleTime,
    artis::traffic::meso::core::LinkGraphManager,
    artis::traffic::meso::core::LinkParameters> _link_6;
  artis::pdevs::Coordinator<artis::common::DoubleTime,
    artis::traffic::meso::core::LinkGraphManager,
    artis::traffic::meso::core::LinkParameters> _link_7;
  artis::pdevs::Coordinator<artis::common::DoubleTime,
    artis::traffic::meso::core::LinkGraphManager,
    artis::traffic::meso::core::LinkParameters> _link_8;
  artis::pdevs::Coordinator<artis::common::DoubleTime,
    artis::traffic::meso::core::LinkGraphManager,
    artis::traffic::meso::core::LinkParameters> _link_9;
  artis::pdevs::Coordinator<artis::common::DoubleTime,
    artis::traffic::meso::core::LinkGraphManager,
    artis::traffic::meso::core::LinkParameters> _link_10;
  artis::pdevs::Coordinator<artis::common::DoubleTime,
    artis::traffic::meso::core::LinkGraphManager,
    artis::traffic::meso::core::LinkParameters> _link_11;
  artis::pdevs::Coordinator<artis::common::DoubleTime,
    artis::traffic::meso::core::LinkGraphManager,
    artis::traffic::meso::core::LinkParameters> _link_12;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::meso::core::Node,
    artis::traffic::meso::core::NodeParameters> _node_3;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::meso::core::Node,
    artis::traffic::meso::core::NodeParameters> _node_4;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::meso::core::Node,
    artis::traffic::meso::core::NodeParameters> _node_5;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::meso::core::Node,
    artis::traffic::meso::core::NodeParameters> _node_6;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::meso::core::Node,
    artis::traffic::meso::core::NodeParameters> _node_9;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::meso::core::Node,
    artis::traffic::meso::core::NodeParameters> _node_10;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::meso::utils::Counter> _counter_2;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::meso::utils::Counter> _counter_4;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::meso::utils::Counter> _counter_7;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::meso::utils::Counter> _counter_12;
};

class TwoLinkWithNodeView : public artis::traffic::core::View {
public:
  TwoLinkWithNodeView() {
    selector("Generator_1:counter",
             {SimpleNetworkGraphManager::GENERATOR_1,
              artis::traffic::meso::utils::Generator::vars::COUNTER});
    selector("Generator_8:counter",
             {SimpleNetworkGraphManager::GENERATOR_8,
              artis::traffic::meso::utils::Generator::vars::COUNTER});
    selector("Generator_11:counter",
             {SimpleNetworkGraphManager::GENERATOR_11,
              artis::traffic::meso::utils::Generator::vars::COUNTER});
    selector("Link_1:vehicle_number",
             {SimpleNetworkGraphManager::LINK_1,
              artis::traffic::meso::core::LinkGraphManager::TRAVEL,
              artis::traffic::meso::core::LinkTravel::vars::VEHICLE_NUMBER});
    selector("Link_2:vehicle_number",
             {SimpleNetworkGraphManager::LINK_2,
              artis::traffic::meso::core::LinkGraphManager::TRAVEL,
              artis::traffic::meso::core::LinkTravel::vars::VEHICLE_NUMBER});
    selector("Link_3:vehicle_number",
             {SimpleNetworkGraphManager::LINK_3,
              artis::traffic::meso::core::LinkGraphManager::TRAVEL,
              artis::traffic::meso::core::LinkTravel::vars::VEHICLE_NUMBER});
    selector("Link_4:vehicle_number",
             {SimpleNetworkGraphManager::LINK_4,
              artis::traffic::meso::core::LinkGraphManager::TRAVEL,
              artis::traffic::meso::core::LinkTravel::vars::VEHICLE_NUMBER});
    selector("Link_5:vehicle_number",
             {SimpleNetworkGraphManager::LINK_5,
              artis::traffic::meso::core::LinkGraphManager::TRAVEL,
              artis::traffic::meso::core::LinkTravel::vars::VEHICLE_NUMBER});
    selector("Link_6:vehicle_number",
             {SimpleNetworkGraphManager::LINK_6,
              artis::traffic::meso::core::LinkGraphManager::TRAVEL,
              artis::traffic::meso::core::LinkTravel::vars::VEHICLE_NUMBER});
    selector("Link_7:vehicle_number",
             {SimpleNetworkGraphManager::LINK_7,
              artis::traffic::meso::core::LinkGraphManager::TRAVEL,
              artis::traffic::meso::core::LinkTravel::vars::VEHICLE_NUMBER});
    selector("Link_8:vehicle_number",
             {SimpleNetworkGraphManager::LINK_8,
              artis::traffic::meso::core::LinkGraphManager::TRAVEL,
              artis::traffic::meso::core::LinkTravel::vars::VEHICLE_NUMBER});
    selector("Link_9:vehicle_number",
             {SimpleNetworkGraphManager::LINK_9,
              artis::traffic::meso::core::LinkGraphManager::TRAVEL,
              artis::traffic::meso::core::LinkTravel::vars::VEHICLE_NUMBER});
    selector("Link_10:vehicle_number",
             {SimpleNetworkGraphManager::LINK_10,
              artis::traffic::meso::core::LinkGraphManager::TRAVEL,
              artis::traffic::meso::core::LinkTravel::vars::VEHICLE_NUMBER});
    selector("Link_11:vehicle_number",
             {SimpleNetworkGraphManager::LINK_11,
              artis::traffic::meso::core::LinkGraphManager::TRAVEL,
              artis::traffic::meso::core::LinkTravel::vars::VEHICLE_NUMBER});
    selector("Link_12:vehicle_number",
             {SimpleNetworkGraphManager::LINK_12,
              artis::traffic::meso::core::LinkGraphManager::TRAVEL,
              artis::traffic::meso::core::LinkTravel::vars::VEHICLE_NUMBER});
    selector("Link_1:total_vehicle_number",
             {SimpleNetworkGraphManager::LINK_1,
              artis::traffic::meso::core::LinkGraphManager::TRAVEL,
              artis::traffic::meso::core::LinkTravel::vars::TOTAL_VEHICLE_NUMBER});
    selector("Link_2:total_vehicle_number",
             {SimpleNetworkGraphManager::LINK_2,
              artis::traffic::meso::core::LinkGraphManager::TRAVEL,
              artis::traffic::meso::core::LinkTravel::vars::TOTAL_VEHICLE_NUMBER});
    selector("Link_3:total_vehicle_number",
             {SimpleNetworkGraphManager::LINK_3,
              artis::traffic::meso::core::LinkGraphManager::TRAVEL,
              artis::traffic::meso::core::LinkTravel::vars::TOTAL_VEHICLE_NUMBER});
    selector("Link_4:total_vehicle_number",
             {SimpleNetworkGraphManager::LINK_4,
              artis::traffic::meso::core::LinkGraphManager::TRAVEL,
              artis::traffic::meso::core::LinkTravel::vars::TOTAL_VEHICLE_NUMBER});
    selector("Link_5:total_vehicle_number",
             {SimpleNetworkGraphManager::LINK_5,
              artis::traffic::meso::core::LinkGraphManager::TRAVEL,
              artis::traffic::meso::core::LinkTravel::vars::TOTAL_VEHICLE_NUMBER});
    selector("Link_6:total_vehicle_number",
             {SimpleNetworkGraphManager::LINK_6,
              artis::traffic::meso::core::LinkGraphManager::TRAVEL,
              artis::traffic::meso::core::LinkTravel::vars::TOTAL_VEHICLE_NUMBER});
    selector("Link_7:total_vehicle_number",
             {SimpleNetworkGraphManager::LINK_7,
              artis::traffic::meso::core::LinkGraphManager::TRAVEL,
              artis::traffic::meso::core::LinkTravel::vars::TOTAL_VEHICLE_NUMBER});
    selector("Link_8:total_vehicle_number",
             {SimpleNetworkGraphManager::LINK_8,
              artis::traffic::meso::core::LinkGraphManager::TRAVEL,
              artis::traffic::meso::core::LinkTravel::vars::TOTAL_VEHICLE_NUMBER});
    selector("Link_9:total_vehicle_number",
             {SimpleNetworkGraphManager::LINK_9,
              artis::traffic::meso::core::LinkGraphManager::TRAVEL,
              artis::traffic::meso::core::LinkTravel::vars::TOTAL_VEHICLE_NUMBER});
    selector("Link_10:total_vehicle_number",
             {SimpleNetworkGraphManager::LINK_10,
              artis::traffic::meso::core::LinkGraphManager::TRAVEL,
              artis::traffic::meso::core::LinkTravel::vars::TOTAL_VEHICLE_NUMBER});
    selector("Link_11:total_vehicle_number",
             {SimpleNetworkGraphManager::LINK_11,
              artis::traffic::meso::core::LinkGraphManager::TRAVEL,
              artis::traffic::meso::core::LinkTravel::vars::TOTAL_VEHICLE_NUMBER});
    selector("Link_12:total_vehicle_number",
             {SimpleNetworkGraphManager::LINK_12,
              artis::traffic::meso::core::LinkGraphManager::TRAVEL,
              artis::traffic::meso::core::LinkTravel::vars::TOTAL_VEHICLE_NUMBER});
    selector("Link_1:waiting_vehicle_number",
             {SimpleNetworkGraphManager::LINK_1,
              artis::traffic::meso::core::LinkGraphManager::QUEUE,
              artis::traffic::meso::core::LinkQueue::vars::VEHICLE_NUMBER});
    selector("Link_2:waiting_vehicle_number",
             {SimpleNetworkGraphManager::LINK_2,
              artis::traffic::meso::core::LinkGraphManager::QUEUE,
              artis::traffic::meso::core::LinkQueue::vars::VEHICLE_NUMBER});
    selector("Link_3:waiting_vehicle_number",
             {SimpleNetworkGraphManager::LINK_3,
              artis::traffic::meso::core::LinkGraphManager::QUEUE,
              artis::traffic::meso::core::LinkQueue::vars::VEHICLE_NUMBER});
    selector("Link_4:waiting_vehicle_number",
             {SimpleNetworkGraphManager::LINK_4,
              artis::traffic::meso::core::LinkGraphManager::QUEUE,
              artis::traffic::meso::core::LinkQueue::vars::VEHICLE_NUMBER});
    selector("Link_5:waiting_vehicle_number",
             {SimpleNetworkGraphManager::LINK_5,
              artis::traffic::meso::core::LinkGraphManager::QUEUE,
              artis::traffic::meso::core::LinkQueue::vars::VEHICLE_NUMBER});
    selector("Link_6:waiting_vehicle_number",
             {SimpleNetworkGraphManager::LINK_6,
              artis::traffic::meso::core::LinkGraphManager::QUEUE,
              artis::traffic::meso::core::LinkQueue::vars::VEHICLE_NUMBER});
    selector("Link_7:waiting_vehicle_number",
             {SimpleNetworkGraphManager::LINK_7,
              artis::traffic::meso::core::LinkGraphManager::QUEUE,
              artis::traffic::meso::core::LinkQueue::vars::VEHICLE_NUMBER});
    selector("Link_8:waiting_vehicle_number",
             {SimpleNetworkGraphManager::LINK_8,
              artis::traffic::meso::core::LinkGraphManager::QUEUE,
              artis::traffic::meso::core::LinkQueue::vars::VEHICLE_NUMBER});
    selector("Link_9:waiting_vehicle_number",
             {SimpleNetworkGraphManager::LINK_9,
              artis::traffic::meso::core::LinkGraphManager::QUEUE,
              artis::traffic::meso::core::LinkQueue::vars::VEHICLE_NUMBER});
    selector("Link_10:waiting_vehicle_number",
             {SimpleNetworkGraphManager::LINK_10,
              artis::traffic::meso::core::LinkGraphManager::QUEUE,
              artis::traffic::meso::core::LinkQueue::vars::VEHICLE_NUMBER});
    selector("Link_11:waiting_vehicle_number",
             {SimpleNetworkGraphManager::LINK_11,
              artis::traffic::meso::core::LinkGraphManager::QUEUE,
              artis::traffic::meso::core::LinkQueue::vars::VEHICLE_NUMBER});
    selector("Link_12:waiting_vehicle_number",
             {SimpleNetworkGraphManager::LINK_12,
              artis::traffic::meso::core::LinkGraphManager::QUEUE,
              artis::traffic::meso::core::LinkQueue::vars::VEHICLE_NUMBER});
    selector("Node_3:arrived",
             {SimpleNetworkGraphManager::NODE_3,
              artis::traffic::meso::core::Node::vars::ARRIVED});
    selector("Node_4:arrived",
             {SimpleNetworkGraphManager::NODE_4,
              artis::traffic::meso::core::Node::vars::ARRIVED});
    selector("Node_5:arrived",
             {SimpleNetworkGraphManager::NODE_5,
              artis::traffic::meso::core::Node::vars::ARRIVED});
    selector("Node_6:arrived",
             {SimpleNetworkGraphManager::NODE_6,
              artis::traffic::meso::core::Node::vars::ARRIVED});
    selector("Node_9:arrived",
             {SimpleNetworkGraphManager::NODE_9,
              artis::traffic::meso::core::Node::vars::ARRIVED});
    selector("Node_10:arrived",
             {SimpleNetworkGraphManager::NODE_10,
              artis::traffic::meso::core::Node::vars::ARRIVED});
    selector("Counter_2:counter",
             {SimpleNetworkGraphManager::COUNTER_2,
              artis::traffic::meso::utils::Counter::vars::COUNTER});
    selector("Counter_4:counter",
             {SimpleNetworkGraphManager::COUNTER_4,
              artis::traffic::meso::utils::Counter::vars::COUNTER});
    selector("Counter_7:counter",
             {SimpleNetworkGraphManager::COUNTER_7,
              artis::traffic::meso::utils::Counter::vars::COUNTER});
    selector("Counter_12:counter",
             {SimpleNetworkGraphManager::COUNTER_12,
              artis::traffic::meso::utils::Counter::vars::COUNTER});
  }
};

class VehiclesView : public artis::traffic::core::View {
public:
  VehiclesView() {
    selector("Counter_2:vehicle",
             {SimpleNetworkGraphManager::COUNTER_2,
              artis::traffic::meso::utils::Counter::vars::VEHICLE});
    selector("Counter_4:vehicle",
             {SimpleNetworkGraphManager::COUNTER_4,
              artis::traffic::meso::utils::Counter::vars::VEHICLE});
    selector("Counter_7:vehicle",
             {SimpleNetworkGraphManager::COUNTER_7,
              artis::traffic::meso::utils::Counter::vars::VEHICLE});
    selector("Counter_12:vehicle",
             {SimpleNetworkGraphManager::COUNTER_12,
              artis::traffic::meso::utils::Counter::vars::VEHICLE});

    selector("Node_3:vehicle",
             {SimpleNetworkGraphManager::NODE_3,
              artis::traffic::meso::core::Node::vars::VEHICLE});
    selector("Node_4:vehicle",
             {SimpleNetworkGraphManager::NODE_4,
              artis::traffic::meso::core::Node::vars::VEHICLE});
    selector("Node_5:vehicle",
             {SimpleNetworkGraphManager::NODE_5,
              artis::traffic::meso::core::Node::vars::VEHICLE});
    selector("Node_6:vehicle",
             {SimpleNetworkGraphManager::NODE_6,
              artis::traffic::meso::core::Node::vars::VEHICLE});
    selector("Node_9:vehicle",
             {SimpleNetworkGraphManager::NODE_9,
              artis::traffic::meso::core::Node::vars::VEHICLE});
    selector("Node_10:vehicle",
             {SimpleNetworkGraphManager::NODE_10,
              artis::traffic::meso::core::Node::vars::VEHICLE});
  }
};

#endif //ARTIS_TRAFFIC_TESTS_MESO_LINKS_GRAPH_MANAGER_HPP
