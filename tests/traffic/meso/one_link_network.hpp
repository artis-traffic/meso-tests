/**
 * @file tests/meso/one_link_network.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef ARTIS_TRAFFIC_MESO_TESTS_ONE_LINK_NETWORK_HPP
#define ARTIS_TRAFFIC_MESO_TESTS_ONE_LINK_NETWORK_HPP

#include <artis-star/kernel/pdevs/Coordinator.hpp>
#include <artis-star/kernel/pdevs/GraphManager.hpp>
#include <artis-star/kernel/pdevs/Simulator.hpp>

#include <artis-traffic/meso/core/Link.hpp>
#include <artis-traffic/meso/core/LinkQueue.hpp>
#include <artis-traffic/meso/core/LinkTravel.hpp>
#include <artis-traffic/meso/utils/End.hpp>
#include <artis-traffic/meso/utils/SimpleGenerator.hpp>

struct OneLinkNetworkParameters {
  artis::traffic::meso::utils::SimpleGeneratorParameters simple_generator_parameters;
  artis::traffic::meso::core::LinkParameters link_parameters;
  artis::traffic::meso::utils::EndParameters end_parameters;
};

class OneLinkNetWorkGraphManager :
  public artis::pdevs::GraphManager<artis::common::DoubleTime, OneLinkNetworkParameters> {
public:
  enum sub_models {
    SIMPLE_GENERATOR, LINK, END
  };

  OneLinkNetWorkGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                             const OneLinkNetworkParameters &parameters,
                             const artis::common::NoParameters &graph_parameters)
    :
    artis::pdevs::GraphManager<artis::common::DoubleTime, OneLinkNetworkParameters>(
      coordinator, parameters, graph_parameters),
    _simple_generator("simple_generator", parameters.simple_generator_parameters),
    _link("link", parameters.link_parameters, graph_parameters),
    _end("end", parameters.end_parameters) {
    add_child(SIMPLE_GENERATOR, &_simple_generator);
    add_child(LINK, &_link);
    add_child(END, &_end);

    // generator <-> link
    out({&_simple_generator, artis::traffic::meso::utils::SimpleGenerator::outputs::OUT})
      >> in({&_link, artis::traffic::meso::core::LinkGraphManager::inputs::IN});
    out({&_link, artis::traffic::meso::core::LinkGraphManager::outputs::OUT_OPEN})
      >> in({&_simple_generator, artis::traffic::meso::utils::SimpleGenerator::inputs::IN_OPEN});
    out({&_link, artis::traffic::meso::core::LinkGraphManager::outputs::OUT_CLOSE})
      >> in({&_simple_generator, artis::traffic::meso::utils::SimpleGenerator::inputs::IN_CLOSE});

    // link <-> end
    out({&_link, artis::traffic::meso::core::LinkGraphManager::outputs::OUT})
      >> in({&_end, artis::traffic::meso::utils::End::inputs::IN});
    out({&_end, artis::traffic::meso::utils::End::outputs::OUT_OPEN})
      >> in({&_link, artis::traffic::meso::core::LinkGraphManager::inputs::IN_OPEN});
    out({&_end, artis::traffic::meso::utils::End::outputs::OUT_CLOSE})
      >> in({&_link, artis::traffic::meso::core::LinkGraphManager::inputs::IN_CLOSE});
    out({&_end, artis::traffic::meso::utils::End::outputs::OUT_CONFIRM})
      >> in({&_link, artis::traffic::meso::core::LinkGraphManager::inputs::IN_CONFIRM_OUT});
  }

  ~OneLinkNetWorkGraphManager() override = default;

private:
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::meso::utils::SimpleGenerator,
    artis::traffic::meso::utils::SimpleGeneratorParameters> _simple_generator;
  artis::pdevs::Coordinator<artis::common::DoubleTime,
    artis::traffic::meso::core::LinkGraphManager,
    artis::traffic::meso::core::LinkParameters> _link;
  artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::meso::utils::End,
    artis::traffic::meso::utils::EndParameters> _end;
};

class OneLinkNetworkView : public artis::traffic::core::View {
public:
  OneLinkNetworkView() {
    selector("Generator:counter",
             {OneLinkNetWorkGraphManager::SIMPLE_GENERATOR,
              artis::traffic::meso::utils::SimpleGenerator::vars::COUNTER});
    selector("Link:vehicle_number",
             {OneLinkNetWorkGraphManager::LINK,
              artis::traffic::meso::core::LinkGraphManager::TRAVEL,
              artis::traffic::meso::core::LinkTravel::vars::VEHICLE_NUMBER});
    selector("Link:total_vehicle_number",
             {OneLinkNetWorkGraphManager::LINK,
              artis::traffic::meso::core::LinkGraphManager::TRAVEL,
              artis::traffic::meso::core::LinkTravel::vars::TOTAL_VEHICLE_NUMBER});
    selector("Link:flow",
             {OneLinkNetWorkGraphManager::LINK,
              artis::traffic::meso::core::LinkGraphManager::TRAVEL,
              artis::traffic::meso::core::LinkTravel::vars::FLOW});
    selector("Link:speed_average",
             {OneLinkNetWorkGraphManager::LINK,
              artis::traffic::meso::core::LinkGraphManager::TRAVEL,
              artis::traffic::meso::core::LinkTravel::vars::SPEED_AVERAGE});
    selector("Link:moving_vehicle_number",
             {OneLinkNetWorkGraphManager::LINK,
              artis::traffic::meso::core::LinkGraphManager::TRAVEL,
              artis::traffic::meso::core::LinkTravel::vars::MOVING_VEHICLE_NUMBER});
    selector("Link:waiting_vehicle_number",
             {OneLinkNetWorkGraphManager::LINK,
              artis::traffic::meso::core::LinkGraphManager::QUEUE,
              artis::traffic::meso::core::LinkQueue::vars::VEHICLE_NUMBER});
    selector("End:total_vehicle_number",
             {OneLinkNetWorkGraphManager::END,
              artis::traffic::meso::utils::End::vars::TOTAL_VEHICLE_NUMBER});
  }
};

#endif //ARTIS_TRAFFIC_MESO_TESTS_ONE_LINK_NETWORK_HPP