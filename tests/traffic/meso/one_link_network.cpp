/**
 * @file tests/meso/one_link_network.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define CATCH_CONFIG_MAIN

#include "one_link_network.hpp"
#include <artis-star/common/RootCoordinator.hpp>
#include <artis-star/common/observer/Output.hpp>
#include <artis-star/common/observer/TimedIterator.hpp>

#include "one_link_network.hpp"
#include <catch2/catch_all.hpp>

#include <chrono>
#include <iostream>

using namespace std::chrono;

/*************************************************
 * Tests
 *************************************************/

TEST_CASE("One link network", "link") {
  OneLinkNetworkParameters parameters = {
    {0,    0.33333},
    {2000, 1, 25, 0.12, 4.2, 0.33333, 1, 1},
    {0.33333}};

  artis::common::context::Context<artis::common::DoubleTime> context(0, 5400);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime,
      OneLinkNetWorkGraphManager,
      OneLinkNetworkParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

  rc.attachView("System", new OneLinkNetworkView);

  rc.switch_to_timed_observer(1);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

//  std::cout << "Duration: " << time_span.count() << std::endl;

  artis::common::observer::Output<artis::common::DoubleTime,
    artis::common::observer::TimedIterator<artis::common::DoubleTime>>
    output(rc.observer());

  output(context.begin(), context.end(), {context.begin(), 1});

  const OneLinkNetworkView::View &view = rc.observer().view("System");
  auto generator_values = view.get<unsigned int>("Generator:counter");
  auto link_values = view.get<unsigned int>("Link:vehicle_number");
  auto link_t_values = view.get<unsigned int>("Link:total_vehicle_number");
  auto link_f_values = view.get<double>("Link:flow");
  auto link_s_values = view.get<double>("Link:speed_average");
  auto link_m_values = view.get<unsigned int>("Link:moving_vehicle_number");
  auto link_w_values = view.get<unsigned int>("Link:waiting_vehicle_number");
  auto end_values = view.get<unsigned int>("End:total_vehicle_number");

  const int delta = 60;

  for (size_t i = delta / 2; i < link_values.size() - delta / 2; ++i) {
    double inflow = double(generator_values[i + delta / 2].second - generator_values[i - delta / 2].second) / delta;
    double outflow = double(end_values[i + delta / 2].second - end_values[i - delta / 2].second) / delta;
    double density = 0;
    double flow = link_f_values[i].second;
    double speed_average = link_s_values[i].second;
    unsigned int moving = link_m_values[i].second;

    for (int k = -delta / 2; k < delta / 2; ++k) {
      density += link_values[i + k].second + link_w_values[i + k].second;
    }
    density /= (double) delta;

    unsigned int g_n = generator_values[i].second;
    unsigned int l_n = link_values[i].second;
    unsigned int l_n_t = link_t_values[i].second;
    unsigned int l_w_n = link_w_values[i].second;
    unsigned int e_n = end_values[i].second;

    std::cout << i << ";" << density / parameters.link_parameters.length << ";" << flow << ";"
              << inflow << ";" << outflow << ";" << g_n << ";" << l_n << ";" << l_n_t << ";" << l_w_n << ";" << e_n
              << ";" << speed_average << ";" << moving << std::endl;

    REQUIRE(true);
  }
}