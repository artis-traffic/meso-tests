#include <artis-traffic/meso/utils/JsonReader.hpp>

#include <artis-star/common/context/Context.hpp>
#include <artis-star/common/RootCoordinator.hpp>
#include <artis-star/common/observer/Output.hpp>
#include <artis-star/common/observer/TimedIterator.hpp>

#include <artis-star/kernel/pdevs/Coordinator.hpp>
#include <artis-star/kernel/pdevs/GraphManager.hpp>
#include <artis-star/kernel/pdevs/Simulator.hpp>

#include <artis-traffic/meso/core/Link.hpp>
#include <artis-traffic/meso/core/Node.hpp>
#include <artis-traffic/meso/utils/Counter.hpp>
#include <artis-traffic/meso/utils/Generator.hpp>

class JSONNetworkGraphManager :
  public artis::pdevs::GraphManager<artis::common::DoubleTime, artis::common::NoParameters, artis::traffic::meso::utils::Network> {
public:
  enum sub_models {
    GENERATOR = 0,
    LINK = 100,
    NODE = 200,
    COUNTER = 300
  };

  JSONNetworkGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                          const artis::common::NoParameters &parameters,
                          const artis::traffic::meso::utils::Network &graph_parameters)
    :
    artis::pdevs::GraphManager<artis::common::DoubleTime, artis::common::NoParameters, artis::traffic::meso::utils::Network>(
      coordinator, parameters, graph_parameters) {
    // inputs
    for (const auto &p: graph_parameters.inputs) {
      artis::traffic::meso::utils::GeneratorParameters generator_parameters{p.second.data.start_index,
                                                                            p.second.data.min, p.second.data.max,
                                                                            p.second.data.mean, p.second.data.stddev,
                                                                            p.second.data.seed, p.second.data.paths};

      _generators[p.first] = new Generator("generator_" + std::to_string(p.first), generator_parameters);
      this->add_child(GENERATOR + p.first, _generators[p.first]);
    }
    // links
    for (const auto &p: graph_parameters.links) {
      unsigned int out_link_number =
        graph_parameters.nodes.find(p.second.destination_nodeID) != graph_parameters.nodes.cend()
        ? graph_parameters.nodes.at(p.second.destination_nodeID).out_link_number : 0;
      artis::traffic::meso::core::LinkParameters link_parameters{p.second.length,
                                                                 p.second.lane_number, p.second.max_speed,
                                                                 p.second.data.concentration, p.second.data.wave_speed,
                                                                 p.second.data.capacity, out_link_number,
                                                                 static_cast<int>(p.first)};

      _links[p.first] = new Link("link_" + std::to_string(p.first), link_parameters, artis::common::NoParameters());
      this->add_child(LINK + p.first, _links[p.first]);
    }
    // nodes
    for (const auto &p: graph_parameters.nodes) {
      artis::traffic::meso::core::NodeParameters node_parameters{p.second.data.transit_duration,
                                                                 p.second.in_link_number, p.second.out_link_number,
                                                                 p.second.data.proportions};

      _nodes[p.first] = new Node("node_" + std::to_string(p.first), node_parameters);
      this->add_child(NODE + p.first, _nodes[p.first]);
    }
    // outputs
    for (const auto &p: graph_parameters.outputs) {
      _counters[p.first] = new Counter("counter_" + std::to_string(p.first), artis::common::NoParameters());
      this->add_child(COUNTER + p.first, _counters[p.first]);
    }
    for (const auto &p: graph_parameters.links) {
      // generator -> link
      if (graph_parameters.inputs.find(p.second.origin_nodeID) != graph_parameters.inputs.cend()) {
        out({_generators[p.second.origin_nodeID], artis::traffic::meso::utils::Generator::outputs::OUT})
          >> in({_links[p.first], artis::traffic::meso::core::LinkGraphManager::inputs::IN});
        out({_links[p.first], artis::traffic::meso::core::LinkGraphManager::outputs::OUT_OPEN})
          >> in({_generators[p.second.origin_nodeID], artis::traffic::meso::utils::Generator::inputs::IN_OPEN});
        out({_links[p.first], artis::traffic::meso::core::LinkGraphManager::outputs::OUT_CLOSE})
          >> in({_generators[p.second.origin_nodeID], artis::traffic::meso::utils::Generator::inputs::IN_CLOSE});
      } else {
        out({_nodes[p.second.origin_nodeID], artis::traffic::meso::core::Node::outputs::OUT})
          >> in({_links[p.first], artis::traffic::meso::core::LinkGraphManager::inputs::IN});
        out({_links[p.first], artis::traffic::meso::core::LinkGraphManager::outputs::OUT_OPEN})
          >> in({_nodes[p.second.origin_nodeID], artis::traffic::meso::core::Node::inputs::IN_OPEN});
        out({_links[p.first], artis::traffic::meso::core::LinkGraphManager::outputs::OUT_CLOSE})
          >> in({_nodes[p.second.origin_nodeID], artis::traffic::meso::core::Node::inputs::IN_CLOSE});
      }
      // link -> counter
      if (graph_parameters.outputs.find(p.second.destination_nodeID) != graph_parameters.outputs.cend()) {
        out({_links[p.first], artis::traffic::meso::core::LinkGraphManager::outputs::OUT})
          >> in({_counters[p.second.destination_nodeID], artis::traffic::meso::utils::Counter::inputs::IN});
        out({_counters[p.second.destination_nodeID], artis::traffic::meso::utils::Counter::outputs::OUT_CONFIRM})
          >> in({_links[p.first], artis::traffic::meso::core::LinkGraphManager::inputs::IN_CONFIRM_OUT});
      } else {
        out({_links[p.first], artis::traffic::meso::core::LinkGraphManager::outputs::OUT})
          >> in({_nodes[p.second.destination_nodeID], artis::traffic::meso::core::Node::inputs::IN});
        out({_nodes[p.second.destination_nodeID], artis::traffic::meso::core::Node::outputs::OUT_CLOSE})
          >> in({_links[p.first], artis::traffic::meso::core::LinkGraphManager::inputs::IN_CLOSE});
        out({_nodes[p.second.destination_nodeID], artis::traffic::meso::core::Node::outputs::OUT_OPEN})
          >> in({_links[p.first], artis::traffic::meso::core::LinkGraphManager::inputs::IN_OPEN});
        out({_nodes[p.second.destination_nodeID], artis::traffic::meso::core::Node::outputs::OUT_CONFIRM})
          >> in({_links[p.first], artis::traffic::meso::core::LinkGraphManager::inputs::IN_CONFIRM_OUT});
      }
    }
  }

  ~JSONNetworkGraphManager() override {
    for (auto p: _generators) { delete p.second; }
    for (auto p: _links) { delete p.second; }
    for (auto p: _nodes) { delete p.second; }
    for (auto p: _counters) { delete p.second; }
  }

private:
  typedef artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::meso::utils::Generator,
    artis::traffic::meso::utils::GeneratorParameters> Generator;
  typedef std::map<unsigned int, Generator *> Generators;
  typedef artis::pdevs::Coordinator<artis::common::DoubleTime,
    artis::traffic::meso::core::LinkGraphManager,
    artis::traffic::meso::core::LinkParameters> Link;
  typedef std::map<unsigned int, Link *> Links;
  typedef artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::meso::core::Node,
    artis::traffic::meso::core::NodeParameters> Node;
  typedef std::map<unsigned int, Node *> Nodes;
  typedef artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::meso::utils::Counter> Counter;
  typedef std::map<unsigned int, Counter *> Counters;

  Generators _generators;
  Links _links;
  Nodes _nodes;
  Counters _counters;
};

class JSONGeneratorView : public artis::traffic::core::View {
public:
  JSONGeneratorView(const artis::traffic::meso::utils::Network& network) {
    for (const auto &p: network.inputs) {
      selector("Generator_1:counter",
               {JSONNetworkGraphManager::GENERATOR + p.first,
                artis::traffic::meso::utils::Generator::vars::COUNTER});
    }
  }
};

class JSONLinkView : public artis::traffic::core::View {
public:
  JSONLinkView(const artis::traffic::meso::utils::Network& network) {
    for (const auto &p: network.links) {
      selector("Link" + std::to_string(p.first) + ":vehicle_number",
               {JSONNetworkGraphManager::LINK + p.first,
                artis::traffic::meso::core::LinkGraphManager::TRAVEL,
                artis::traffic::meso::core::LinkTravel::vars::VEHICLE_NUMBER});
      selector("Link" + std::to_string(p.first) + ":total_vehicle_number",
               {JSONNetworkGraphManager::LINK + p.first,
                artis::traffic::meso::core::LinkGraphManager::TRAVEL,
                artis::traffic::meso::core::LinkTravel::vars::TOTAL_VEHICLE_NUMBER});
      selector("Link" + std::to_string(p.first) + ":waiting_vehicle_number",
               {JSONNetworkGraphManager::LINK + p.first,
                artis::traffic::meso::core::LinkGraphManager::QUEUE,
                artis::traffic::meso::core::LinkQueue::vars::VEHICLE_NUMBER});
    }
  }
};

class JSONNodeView : public artis::traffic::core::View {
public:
  JSONNodeView(const artis::traffic::meso::utils::Network &network) {
    for (const auto &p: network.nodes) {
      selector("Node" + std::to_string(p.first) + ":arrived",
               {JSONNetworkGraphManager::NODE + p.first,
                artis::traffic::meso::core::Node::vars::ARRIVED});
    }
  }
};

class JSONCounterView : public artis::traffic::core::View {
public:
  JSONCounterView(const artis::traffic::meso::utils::Network& network) {
    for (const auto &p: network.outputs) {
      selector("Counter" + std::to_string(p.first) + ":counter",
               {JSONNetworkGraphManager::COUNTER + p.first,
                artis::traffic::meso::utils::Counter::vars::COUNTER});
    }
  }
};

int main() {
  std::ifstream input("../../../../data/network_1.json");

  if (input) {
    std::string str((std::istreambuf_iterator<char>(input)), std::istreambuf_iterator<char>());

//  std::string str = R"({"level":"meso","inputs":[)";
//
//  str += R"({"ID":1,"start index":0,"min":0.1,"max":0.3,"mean":0.2,"stddev":0.01,"seed":432451,"paths":[[0,0,0],[0,1,0]]},)";
//  str += R"({"ID":8,"start index":0,"min":0.1,"max":0.3,"mean":0.2,"stddev":0.01,"seed":432452,"paths":[[0,0,0],[0,1,0]]},)";
//  str += R"({"ID":11,"start index":0,"min":0.1,"max":0.3,"mean":0.2,"stddev":0.01,"seed":432453,"paths":[[0,0,0],[0,1,0]]})";
//  str += R"(],"outputs":[)";
//  str += R"({"ID":2,"type":"counter"},{"ID":7,"type":"counter"},{"ID":12,"type":"counter"},{"ID":13,"type":"counter"})";
//  str += R"(],)";
//  str += R"("nodes":[)";
//  str += R"({"ID":3,"transit duration":0,"proportions":[1]},{"ID":4,"transit duration":0,"proportions":[1]},{"ID":5,"transit duration":0,"proportions":[0.75,0.25]},{"ID":6,"transit duration":0,"proportions":[1]},)";
//  str += R"({"ID":9,"transit duration":0,"proportions":[1]},{"ID":10,"transit duration":0,"proportions":[0.5,0.5]})";
//  str += R"(])";
//  str += R"(,"links":[)";
//  str += R"({"ID":1,"origin":11,"destination":9,"length":838.9045422558034,"max speed":25,"lane number":2,"concentration":0.12,"wave speed":6.03448275862069,"capacity":1.1666666666666667},)";
//  str += R"({"ID":2,"origin":9,"destination":5,"length":325.76532757685436,"max speed":13.88888888888889,"lane number":1,"concentration":0.14,"wave speed":3.7878787878787876,"capacity":1.1666666666666667},)";
//  str += R"({"ID":3,"origin":9,"destination":13,"length":649.5288300368213,"max speed":13.88888888888889,"lane number":1,"concentration":0.14,"wave speed":3.7878787878787876,"capacity":1.1666666666666667},)";
//  str += R"({"ID":4,"origin":8,"destination":6,"length":296.1776578416324,"max speed":30.555555555555554,"lane number":2,"concentration":0.12,"wave speed":5.780780780780781,"capacity":1.1666666666666667},)";
//  str += R"({"ID":5,"origin":6,"destination":10,"length":385.0239747913269,"max speed":13.88888888888889,"lane number":1,"concentration":0.14,"wave speed":3.7878787878787876,"capacity":0.4166666666666667},)";
//  str += R"({"ID":6,"origin":6,"destination":5,"length":470.47634439116223,"max speed":30.555555555555554,"lane number":2,"concentration":0.12,"wave speed":5.780780780780781,"capacity":1.1666666666666667},)";
//  str += R"({"ID":7,"origin":5,"destination":2,"length":521.1159478447717,"max speed":30.555555555555554,"lane number":2,"concentration":0.12,"wave speed":5.780780780780781,"capacity":1.1666666666666667},)";
//  str += R"({"ID":8,"origin":1,"destination":3,"length":308.4873243331674,"max speed":30.555555555555554,"lane number":2,"concentration":0.12,"wave speed":5.780780780780781,"capacity":1.1666666666666667},)";
//  str += R"({"ID":9,"origin":3,"destination":10,"length":788.416249620489,"max speed":13.88888888888889,"lane number":1,"concentration":0.14,"wave speed":3.7878787878787876,"capacity":0.4166666666666667},)";
//  str += R"({"ID":10,"origin":10,"destination":12,"length":742.0905061522807,"max speed":25,"lane number":2,"concentration":0.12,"wave speed":6.03448275862069,"capacity":1.1666666666666667},)";
//  str += R"({"ID":11,"origin":3,"destination":4,"length":269.5449579184339,"max speed":30.555555555555554,"lane number":2,"concentration":0.12,"wave speed":5.780780780780781,"capacity":1.1666666666666667},)";
//  str += R"({"ID":12,"origin":4,"destination":7,"length":701.7882356491866,"max speed":30.555555555555554,"lane number":2,"concentration":0.12,"wave speed":5.780780780780781,"capacity":1.1666666666666667})";
//  str += R"(]})";

    artis::traffic::meso::utils::JsonReader reader;

    reader.parse_network(str);

    const artis::traffic::meso::utils::Network &network = reader.network();

    artis::common::context::Context<artis::common::DoubleTime> context(25200, 27000);
    artis::common::RootCoordinator<
      artis::common::DoubleTime, artis::pdevs::Coordinator<
        artis::common::DoubleTime,
        JSONNetworkGraphManager,
        artis::common::NoParameters,
        artis::traffic::meso::utils::Network>
    > rc(context, "root", artis::common::NoParameters(), network);

    rc.attachView("Generator", new JSONGeneratorView(network));
    rc.attachView("Link", new JSONLinkView(network));
    rc.attachView("Node", new JSONNodeView(network));
    rc.attachView("Counter", new JSONCounterView(network));
    rc.switch_to_timed_observer(0.1);

    std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();

    rc.run(context);
    std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();

    std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double> >(t2 - t1);

    std::cout << "Duration: " << time_span.count() << std::endl;

    artis::common::observer::Output<artis::common::DoubleTime,
      artis::common::observer::TimedIterator<artis::common::DoubleTime>>
      output(rc.observer());

    output(context.begin(), context.end(), {context.begin(), 0.1});
  }
  return 0;
}