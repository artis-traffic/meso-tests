/**
 * @file tests/meso/links.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define CATCH_CONFIG_MAIN

#include <artis-star/common/RootCoordinator.hpp>
#include <artis-star/common/observer/Output.hpp>
#include <artis-star/common/observer/TimedIterator.hpp>

#include "links_def.hpp"

#include <catch2/catch_all.hpp>

#include <chrono>

using namespace std::chrono;

/*************************************************
 * Tests
 *************************************************/

TEST_CASE("simple network", "links")
{
  SimpleNetworkParameters parameters = {{0,                  0.1, 0.2,                 0.3,  0.01,                 432451,             {{0, 0, 0}, {0, 1, 0}}},
                                        {10000,              0.1, 0.2,                 0.3,  0.01,                 432452,             {{0, 0, 0}, {0, 1, 0}}},
                                        {20000,              0.1, 0.2,                 0.3,  0.01,                 432453,             {{0, 0, 0}, {0, 1, 0}}},
                                        {838.9045422558034,  2, 25,                 0.12, 6.03448275862069,
                                                                                                              1.1666666666666667, 2, 1},
                                        {325.76532757685436, 1, 13.88888888888889,  0.14,
                                                                                          3.7878787878787876, 1.1666666666666667, 1, 2},
                                        {649.5288300368213,  1, 13.88888888888889,  0.14,
                                                                                          3.7878787878787876, 1.1666666666666667, 0, 3},
                                        {296.1776578416324,  2, 30.555555555555554, 0.12,
                                                                                          5.780780780780781,  1.1666666666666667, 2, 4},
                                        {385.0239747913269,  1, 13.88888888888889,  0.14,
                                                                                          3.7878787878787876, 0.4166666666666667, 1, 5},
                                        {470.47634439116223, 2, 30.555555555555554, 0.12,
                                                                                          5.780780780780781,  1.1666666666666667, 1, 6},
                                        {521.1159478447717,  2, 30.555555555555554, 0.12,
                                                                                          5.780780780780781,  1.1666666666666667, 0, 7},
                                        {308.4873243331674,  2, 30.555555555555554, 0.12,
                                                                                          5.780780780780781,  1.1666666666666667, 2, 8},
                                        {788.416249620489,   1, 13.88888888888889,  0.14,
                                                                                          3.7878787878787876, 0.4166666666666667, 1, 9},
                                        {742.0905061522807,  2, 25,                 0.12, 6.03448275862069,
                                                                                                              1.1666666666666667, 0, 10},
                                        {269.5449579184339,  2, 30.555555555555554, 0.12,
                                                                                          5.780780780780781,  1.1666666666666667, 1, 11},
                                        {701.7882356491866,  2, 30.555555555555554, 0.12,
                                                                                          5.780780780780781,  1.1666666666666667, 0, 12},
                                        {0.,                 1, 2,                  {1}},
                                        {0.,                 1, 1,                  {1}},
                                        {0.,                 2, 1,                  {0.75, 0.25}},
                                        {0.,                 1, 2,                  {1}},
                                        {0.,                 1, 2,                  {1}},
                                        {0.,                 2, 1,                  {0.5,  0.5}}};
  artis::common::context::Context<artis::common::DoubleTime> context(25200, 27000);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime,
      SimpleNetworkGraphManager,
      SimpleNetworkParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

  rc.attachView("System", new TwoLinkWithNodeView);
  rc.attachView("Vehicles", new VehiclesView);

  rc.switch_to_timed_observer(0.1);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

  artis::common::observer::Output<artis::common::DoubleTime,
    artis::common::observer::TimedIterator<artis::common::DoubleTime>>
    output(rc.observer());

  output(context.begin(), context.end(), {context.begin(), 0.1});

//  artis::observer::Output<artis::common::DoubleTime,
//    artis::observer::EventIterator<artis::common::DoubleTime>>
//    output(rc.observer());
//  output(context.begin(), context.end(), {});

  const TwoLinkWithNodeView::View &view = rc.observer().view("System");
  auto generator_1_values = view.get<unsigned int>("Generator_1:counter");
  auto generator_8_values = view.get<unsigned int>("Generator_8:counter");
  auto generator_11_values = view.get<unsigned int>("Generator_11:counter");
  auto link_1_values = view.get<unsigned int>("Link_1:vehicle_number");
  auto link_2_values = view.get<unsigned int>("Link_2:vehicle_number");
  auto link_3_values = view.get<unsigned int>("Link_3:vehicle_number");
  auto link_4_values = view.get<unsigned int>("Link_4:vehicle_number");
  auto link_5_values = view.get<unsigned int>("Link_5:vehicle_number");
  auto link_6_values = view.get<unsigned int>("Link_6:vehicle_number");
  auto link_7_values = view.get<unsigned int>("Link_7:vehicle_number");
  auto link_8_values = view.get<unsigned int>("Link_8:vehicle_number");
  auto link_9_values = view.get<unsigned int>("Link_9:vehicle_number");
  auto link_10_values = view.get<unsigned int>("Link_10:vehicle_number");
  auto link_11_values = view.get<unsigned int>("Link_11:vehicle_number");
  auto link_12_values = view.get<unsigned int>("Link_12:vehicle_number");
  auto link_1_t_values = view.get<unsigned int>("Link_1:total_vehicle_number");
  auto link_2_t_values = view.get<unsigned int>("Link_2:total_vehicle_number");
  auto link_3_t_values = view.get<unsigned int>("Link_3:total_vehicle_number");
  auto link_4_t_values = view.get<unsigned int>("Link_4:total_vehicle_number");
  auto link_5_t_values = view.get<unsigned int>("Link_5:total_vehicle_number");
  auto link_6_t_values = view.get<unsigned int>("Link_6:total_vehicle_number");
  auto link_7_t_values = view.get<unsigned int>("Link_7:total_vehicle_number");
  auto link_8_t_values = view.get<unsigned int>("Link_8:total_vehicle_number");
  auto link_9_t_values = view.get<unsigned int>("Link_9:total_vehicle_number");
  auto link_10_t_values = view.get<unsigned int>("Link_10:total_vehicle_number");
  auto link_11_t_values = view.get<unsigned int>("Link_11:total_vehicle_number");
  auto link_12_t_values = view.get<unsigned int>("Link_12:total_vehicle_number");
  auto link_1_w_values = view.get<unsigned int>("Link_1:waiting_vehicle_number");
  auto link_2_w_values = view.get<unsigned int>("Link_2:waiting_vehicle_number");
  auto link_3_w_values = view.get<unsigned int>("Link_3:waiting_vehicle_number");
  auto link_4_w_values = view.get<unsigned int>("Link_4:waiting_vehicle_number");
  auto link_5_w_values = view.get<unsigned int>("Link_5:waiting_vehicle_number");
  auto link_6_w_values = view.get<unsigned int>("Link_6:waiting_vehicle_number");
  auto link_7_w_values = view.get<unsigned int>("Link_7:waiting_vehicle_number");
  auto link_8_w_values = view.get<unsigned int>("Link_8:waiting_vehicle_number");
  auto link_9_w_values = view.get<unsigned int>("Link_9:waiting_vehicle_number");
  auto link_10_w_values = view.get<unsigned int>("Link_10:waiting_vehicle_number");
  auto link_11_w_values = view.get<unsigned int>("Link_11:waiting_vehicle_number");
  auto link_12_w_values = view.get<unsigned int>("Link_12:waiting_vehicle_number");
  auto node_3_values = view.get<unsigned int>("Node_3:arrived");
  auto node_4_values = view.get<unsigned int>("Node_4:arrived");
  auto node_5_values = view.get<unsigned int>("Node_5:arrived");
  auto node_6_values = view.get<unsigned int>("Node_6:arrived");
  auto node_9_values = view.get<unsigned int>("Node_9:arrived");
  auto node_10_values = view.get<unsigned int>("Node_10:arrived");
  auto counter_2_values = view.get<unsigned int>("Counter_2:counter");
  auto counter_4_values = view.get<unsigned int>("Counter_4:counter");
  auto counter_7_values = view.get<unsigned int>("Counter_7:counter");
  auto counter_12_values = view.get<unsigned int>("Counter_12:counter");

  for (size_t i = 0; i < link_1_values.size(); ++i) {
    unsigned int g_n_1 = generator_1_values[i].second, g_n_8 = generator_8_values[i].second,
      g_n_11 = generator_11_values[i].second;
    unsigned int c_n_2 = counter_2_values[i].second, c_n_4 = counter_4_values[i].second,
      c_n_7 = counter_7_values[i].second, c_n_12 = counter_12_values[i].second;
    unsigned int l_n_1 = link_1_values[i].second, l_n_2 = link_2_values[i].second,
      l_n_3 = link_3_values[i].second, l_n_4 = link_4_values[i].second,
      l_n_5 = link_5_values[i].second, l_n_6 = link_6_values[i].second,
      l_n_7 = link_7_values[i].second, l_n_8 = link_8_values[i].second,
      l_n_9 = link_9_values[i].second, l_n_10 = link_10_values[i].second,
      l_n_11 = link_11_values[i].second, l_n_12 = link_12_values[i].second;
    unsigned int l_n_t_1 = link_1_t_values[i].second, l_n_t_2 = link_2_t_values[i].second,
      l_n_t_3 = link_3_t_values[i].second, l_n_t_4 = link_4_t_values[i].second,
      l_n_t_5 = link_5_t_values[i].second, l_n_t_6 = link_6_t_values[i].second,
      l_n_t_7 = link_7_t_values[i].second, l_n_t_8 = link_8_t_values[i].second,
      l_n_t_9 = link_9_t_values[i].second, l_n_t_10 = link_10_t_values[i].second,
      l_n_t_11 = link_11_t_values[i].second, l_n_t_12 = link_12_t_values[i].second;
    unsigned int l_w_n_1 = link_1_w_values[i].second, l_w_n_2 = link_2_w_values[i].second,
      l_w_n_3 = link_3_w_values[i].second, l_w_n_4 = link_4_w_values[i].second,
      l_w_n_5 = link_5_w_values[i].second, l_w_n_6 = link_6_w_values[i].second,
      l_w_n_7 = link_7_w_values[i].second, l_w_n_8 = link_8_w_values[i].second,
      l_w_n_9 = link_9_w_values[i].second, l_w_n_10 = link_10_w_values[i].second,
      l_w_n_11 = link_11_w_values[i].second, l_w_n_12 = link_12_w_values[i].second;
    unsigned int n_n_3 = node_3_values[i].second, n_n_4 = node_4_values[i].second,
      n_n_5 = node_5_values[i].second, n_n_6 = node_6_values[i].second,
      n_n_9 = node_9_values[i].second, n_n_10 = node_10_values[i].second;

//    std::cout << link_1_w_values[i].first << " " << l_w_n_1 << " " << l_w_n_2 << " " << l_w_n_3
//              << " " << l_w_n_4 << " " << l_w_n_5 << " " << l_w_n_6 << " " << l_w_n_7 << " "
//              << l_w_n_8 << " " << l_w_n_9 << " " << l_w_n_10 << " " << l_w_n_11 << " " << l_w_n_12
//              << " " << c_n_2 << " " << c_n_4 << " " << c_n_7 << " " << c_n_12
//              << " " << (g_n_1 + g_n_8 + g_n_11)
//              << std::endl;

    std::cout << link_1_w_values[i].first << " " << l_n_1 << " " << l_n_2 << " " << l_n_3
              << " " << l_n_4 << " " << l_n_5 << " " << l_n_6 << " " << l_n_7 << " "
              << l_n_8 << " " << l_n_9 << " " << l_n_10 << " " << l_n_11 << " " << l_n_12
              << " " << l_n_t_1 << " " << l_n_t_2 << " " << l_n_t_3
              << " " << l_n_t_4 << " " << l_n_t_5 << " " << l_n_t_6 << " " << l_n_t_7 << " "
              << l_n_t_8 << " " << l_n_t_9 << " " << l_n_t_10 << " " << l_n_t_11 << " " << l_n_t_12
              << std::endl;

    REQUIRE(g_n_1 + g_n_8 + g_n_11
            == l_n_1 + l_w_n_1 + l_n_2 + l_w_n_2 + l_n_3 + l_w_n_3 + l_n_4 + l_w_n_4 + l_n_5
               + l_w_n_5 + l_n_6 + l_w_n_6 + l_n_7 + l_w_n_7 + l_n_8 + l_w_n_8 + l_n_9
               + l_w_n_9 + l_n_10 + l_w_n_10 + l_n_11 + l_w_n_11 + l_n_12 + l_w_n_12 + n_n_3
               + n_n_4 + n_n_5 + n_n_6 + n_n_9 + n_n_10 + c_n_2 + c_n_4 + c_n_7 + c_n_12);
  }
}