/**
 * @file tests/meso/linear_network.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_TESTS_MESO_LINEAR_NETWORK_HPP
#define ARTIS_TRAFFIC_TESTS_MESO_LINEAR_NETWORK_HPP

#include <artis-star/kernel/pdevs/Coordinator.hpp>
#include <artis-star/kernel/pdevs/GraphManager.hpp>
#include <artis-star/kernel/pdevs/Simulator.hpp>
#include <memory>

#include <artis-traffic/meso/core/Link.hpp>
#include <artis-traffic/meso/core/Node.hpp>
#include <artis-traffic/meso/utils/Counter.hpp>
#include <artis-traffic/meso/utils/Generator.hpp>

struct LinearNetworkGraphManagerParameters {
  size_t line_number;
  size_t link_number;
};

struct LinearNetworkParameters {
  std::vector<artis::traffic::meso::utils::GeneratorParameters> generator_parameters;
  std::vector<std::vector<artis::traffic::meso::core::LinkParameters>> link_parameters;
  std::vector<std::vector<artis::traffic::meso::core::NodeParameters>> node_parameters;
};

class LinearNetworkGraphManager :
  public artis::pdevs::GraphManager<artis::common::DoubleTime,
    LinearNetworkParameters,
    LinearNetworkGraphManagerParameters> {
public:
  enum sub_models {
    GENERATOR = 0,
    LINK = 1000,
    NODE = 2000,
    COUNTER = 3000
  };

  LinearNetworkGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                            const LinearNetworkParameters &parameters,
                            const LinearNetworkGraphManagerParameters &graph_parameters)
    :
    artis::pdevs::GraphManager<artis::common::DoubleTime,
      LinearNetworkParameters,
      LinearNetworkGraphManagerParameters>(
      coordinator, parameters, graph_parameters) {
    // ** models **
    // generators
    for (size_t i = 0; i < graph_parameters.line_number; ++i) {
      std::stringstream ss;

      ss << "generator_" << (i + 1);
      _generators.push_back(std::make_shared<GeneratorSimulator>(ss.str(), parameters.generator_parameters[i]));
      add_child(GENERATOR + i, _generators.back().get());
    }
    // links
    for (size_t i = 0; i < graph_parameters.line_number; ++i) {
      _links.emplace_back();
      for (size_t j = 0; j < graph_parameters.link_number; ++j) {
        std::stringstream ss;

        ss << "link_" << (i + 1) << "_" << (j + 1);
        _links.back().push_back(std::make_shared<LinkCoordinator>(ss.str(), parameters.link_parameters[i][j], artis::common::NoParameters()));
        add_child(LINK + i * graph_parameters.link_number + j, _links.back().back().get());
      }
    }
    // nodes
    if (graph_parameters.link_number > 1) {
      for (size_t i = 0; i < graph_parameters.line_number; ++i) {
        _nodes.emplace_back();
        for (size_t j = 0; j < graph_parameters.link_number - 1; ++j) {
          std::stringstream ss;

          ss << "node_" << (i + 1) << "_" << (j + 1);
          _nodes.back().push_back(std::make_shared<NodeSimulator>(ss.str(), parameters.node_parameters[i][j]));
          add_child(NODE + i * graph_parameters.link_number + j, _nodes.back().back().get());
        }
      }
    }
    // counters
    for (size_t i = 0; i < graph_parameters.line_number; ++i) {
      std::stringstream ss;

      ss << "counter_" << (i + 1);
      _counters.push_back(std::make_shared<CounterSimulator>(ss.str(), artis::common::NoParameters()));
      add_child(COUNTER + i, _counters.back().get());
    }
    // ** connections **
    // generator -> link
    for (size_t i = 0; i < graph_parameters.line_number; ++i) {
      connect_generator_to_link(_generators[i].get(), _links[i].front().get());
    }
    if (graph_parameters.link_number > 1) {
      // link -> node
      for (size_t i = 0; i < graph_parameters.line_number; ++i) {
        for (size_t j = 0; j < graph_parameters.link_number - 1; ++j) {
          connect_link_to_node(_links[i][j].get(), 0, _nodes[i][j].get());
        }
      }
      // node -> link
      for (size_t i = 0; i < graph_parameters.line_number; ++i) {
        for (size_t j = 0; j < graph_parameters.link_number - 1; ++j) {
          connect_node_to_link(_nodes[i][j].get(), _links[i][j + 1].get(), 0);
        }
      }
    }
    // link -> counter
    for (size_t i = 0; i < graph_parameters.line_number; ++i) {
      connect_link_to_counter(_links[i].back().get(), _counters[i].get());
    }
  }

  void init() {}

  artis::common::DoubleTime::type
  lookahead(const artis::common::DoubleTime::type &t) const override {
    std::vector<double> lookaheads;

    for (const auto &g: _generators) {
      lookaheads.push_back(g->lookahead(t));
    }
    for (const auto &ll: _links) {
      for (const auto &l: ll) {
        lookaheads.push_back(l->lookahead(t));
      }
    }
    for (const auto &nl: _nodes) {
      for (const auto &n: nl) {
        lookaheads.push_back(n->lookahead(t));
      }
    }
    return *std::min(lookaheads.begin(), lookaheads.end());
  }

  ~LinearNetworkGraphManager() override = default;

private:
  typedef artis::pdevs::Simulator<
    artis::common::DoubleTime,
    artis::traffic::meso::utils::Generator,
    artis::traffic::meso::utils::GeneratorParameters> GeneratorSimulator;
  typedef artis::pdevs::Coordinator<
    artis::common::DoubleTime,
    artis::traffic::meso::core::LinkGraphManager,
    artis::traffic::meso::core::LinkParameters> LinkCoordinator;
  typedef artis::pdevs::Simulator<
    artis::common::DoubleTime,
    artis::traffic::meso::core::Node,
    artis::traffic::meso::core::NodeParameters> NodeSimulator;
  typedef artis::pdevs::Simulator<artis::common::DoubleTime,
    artis::traffic::meso::utils::Counter> CounterSimulator;

  void connect_generator_to_link(GeneratorSimulator *generator, LinkCoordinator *link) {
    out({generator, artis::traffic::meso::utils::Generator::outputs::OUT})
      >> in({link, artis::traffic::meso::core::LinkGraphManager::inputs::IN});
  }

  void connect_link_to_node(LinkCoordinator *link, unsigned int link_index, NodeSimulator *node) {
    out({link, artis::traffic::meso::core::LinkGraphManager::outputs::OUT})
      >> in({node, artis::traffic::meso::core::Node::inputs::IN + link_index});
    out({node, artis::traffic::meso::core::Node::outputs::OUT_CLOSE})
      >> in({link, artis::traffic::meso::core::LinkGraphManager::inputs::IN_CLOSE});
    out({node, artis::traffic::meso::core::Node::outputs::OUT_OPEN})
      >> in({link, artis::traffic::meso::core::LinkGraphManager::inputs::IN_OPEN});
    out({node, artis::traffic::meso::core::Node::outputs::BACK + link_index})
      >> in({link, artis::traffic::meso::core::LinkGraphManager::inputs::BACK});
  }

  void connect_node_to_link(NodeSimulator *node, LinkCoordinator *link, unsigned int link_index) {
    out({node, artis::traffic::meso::core::Node::outputs::OUT + link_index})
      >> in({link, artis::traffic::meso::core::LinkGraphManager::inputs::IN});
    out({link, artis::traffic::meso::core::LinkGraphManager::outputs::OUT_OPEN})
      >> in({node, artis::traffic::meso::core::Node::inputs::IN_OPEN + link_index});
    out({link, artis::traffic::meso::core::LinkGraphManager::outputs::OUT_CLOSE})
      >> in({node, artis::traffic::meso::core::Node::inputs::IN_CLOSE + link_index});
  }

  void connect_link_to_counter(LinkCoordinator *link, CounterSimulator *counter) {
    out({link, artis::traffic::meso::core::LinkGraphManager::outputs::OUT})
      >> in({counter, artis::traffic::meso::utils::Counter::inputs::IN});
  }

  std::vector<std::shared_ptr<GeneratorSimulator>> _generators;
  std::vector<std::vector<std::shared_ptr<LinkCoordinator>>> _links;
  std::vector<std::vector<std::shared_ptr<NodeSimulator>>> _nodes;
  std::vector<std::shared_ptr<CounterSimulator>> _counters;
};

struct NetworkGraphManagerParameters {
  size_t network_number;
  LinearNetworkGraphManagerParameters parameters;
};

class NetworkGraphManager :
  public artis::pdevs::GraphManager<artis::common::DoubleTime,
    LinearNetworkParameters,
    NetworkGraphManagerParameters> {
public:
  enum submodels {
    NETWORK
  };

  typedef artis::pdevs::multithreading::Coordinator<
    artis::common::DoubleTime,
    NetworkGraphManager,
    LinearNetworkParameters,
    NetworkGraphManagerParameters> coordinator_type;

  NetworkGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                      const LinearNetworkParameters &parameters,
                      const NetworkGraphManagerParameters &graph_parameters)
    :
    artis::pdevs::GraphManager<artis::common::DoubleTime,
      LinearNetworkParameters,
      NetworkGraphManagerParameters>(
      coordinator, parameters, graph_parameters) {
    // ** models **
    for (size_t i = 0; i < graph_parameters.network_number; ++i) {
      std::stringstream ss;

      ss << "network_" << (i + 1);
      networks
        .push_back(std::make_shared<Network>(ss.str(), parameters, graph_parameters.parameters));
      add_child(NETWORK + i, networks.back().get());
    }
  }

  void init() {
    for (auto &c: networks) {
      dynamic_cast< coordinator_type *>(this->coordinator())->attach_child(c.get(), c->get_queue());
      c->attach_parent(dynamic_cast< coordinator_type *>(this->coordinator())->get_queue());
    }
  }

  artis::common::DoubleTime::type
  lookahead(
    const artis::common::DoubleTime::type &t) const override {
    artis::common::DoubleTime::type
      min = std::numeric_limits<artis::common::DoubleTime::type>::max();

    for (const auto &p: networks) {
      artis::common::DoubleTime::type l = p->lookahead(t);

      if (min > l) {
        min = l;
      }
    }
    return min;
  }

  ~NetworkGraphManager() override = default;

private:
  typedef artis::pdevs::multithreading::Coordinator<artis::common::DoubleTime,
    LinearNetworkGraphManager,
    LinearNetworkParameters,
    LinearNetworkGraphManagerParameters> Network;

  std::vector<std::shared_ptr<Network> > networks;
};

#endif //ARTIS_TRAFFIC_TESTS_MESO_LINEAR_NETWORK_HPP
