/**
 * @file tests/meso/linear_network.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-star/common/RootCoordinator.hpp>
#include <artis-star/kernel/pdevs/multithreading/Coordinator.hpp>

#include "linear_network.hpp"

#include <chrono>

using namespace std::chrono;

/*************************************************
 * Tests
 *************************************************/

int main()
{
  LinearNetworkGraphManagerParameters graph_manager_parameters{4, 100};
  LinearNetworkParameters parameters;
  std::vector<std::vector<unsigned int>> paths;

  for (size_t i = 0; i < graph_manager_parameters.line_number; ++i) {
    std::vector<unsigned int> path;

    for (size_t j = 0; j < graph_manager_parameters.link_number; ++j) {
      path.push_back(0);
    }
    paths.push_back(path);
  }
  for (unsigned int i = 0; i < graph_manager_parameters.line_number; ++i) {
    parameters.generator_parameters.push_back(
        {(unsigned int) (i * 10000), 1.5, 4.5, 3., 1., 3874 + i * 35, paths});
  }
  for (unsigned int i = 0; i < graph_manager_parameters.line_number; ++i) {
    parameters.link_parameters.emplace_back();
    for (unsigned int j = 0; j < graph_manager_parameters.link_number; ++j) {
      parameters.link_parameters.back().push_back(
          {500, 1, 25, 0.2, 6,
           1, static_cast<unsigned int>(j == graph_manager_parameters.link_number - 1 ? 0 : 1),
           (int) (i * graph_manager_parameters.link_number + j + 1)});
    }
  }
  if (graph_manager_parameters.link_number > 1) {
    for (unsigned int i = 0; i < graph_manager_parameters.line_number; ++i) {
      parameters.node_parameters.emplace_back();
      for (unsigned int j = 0; j < graph_manager_parameters.link_number - 1; ++j) {
        parameters.node_parameters.back().push_back(
            {0., 1, 1, {1}});
      }
    }
  }

  artis::common::context::Context<artis::common::DoubleTime> context(0, 1000);

//  graph_manager_parameters.line_number /= 2;
//  NetworkGraphManagerParameters network_graph_manager_parameters{2, graph_manager_parameters};
//  artis::common::RootCoordinator<
//      artis::common::DoubleTime, artis::pdevs::multithreading::Coordinator<
//          artis::common::DoubleTime,
//          NetworkGraphManager,
//          LinearNetworkParameters,
//          NetworkGraphManagerParameters>
//  > rc(context, "root", parameters, network_graph_manager_parameters);

  artis::common::RootCoordinator<
      artis::common::DoubleTime, artis::pdevs::Coordinator<
          artis::common::DoubleTime,
          LinearNetworkGraphManager,
          LinearNetworkParameters,
          LinearNetworkGraphManagerParameters>
  > rc(context, "root", parameters, graph_manager_parameters);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration = " << time_span.count() << std::endl;

  return 0;
}